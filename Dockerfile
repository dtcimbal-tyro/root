FROM alpine:3.7

RUN addgroup -g 1234 user-grp

RUN adduser -S -u 1234 user -G user-grp

USER 1234

ENTRYPOINT ["cat","/tmp/shadow"]
